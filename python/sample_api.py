from flask import Flask, jsonify

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "Hello, World!"


@app.route("/api/v1/users")
def get_users():
    users = [
        {"id": 1, "name": "John Doe", "email": "john.doe@example.com"},
        {"id": 2, "name": "Jane Doe", "email": "jane.doe@example.com"},
    ]
    return jsonify(users)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8080)
