resource "aws_security_group" "sample-alb-sg" {
  name        = "allow_http"
  description = "Allow HTTP traffic"
  vpc_id      = aws_vpc.sample-vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "sample-app-sg" {
  name        = "allow_sample_api"
  description = "Allow HTTP traffic"
  vpc_id      = aws_vpc.sample-vpc.id

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = [aws_security_group.sample-alb-sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_vpc" "sample-vpc" {
  cidr_block = "192.168.0.0/20"
  tags = {
    Name = "sample-vpc"
  }
}

resource "aws_internet_gateway" "sample-igw" {
  vpc_id = aws_vpc.sample-vpc.id

  tags = {
    Name = "sample-igw"
  }
}

resource "aws_subnet" "sample-subnet-a" {
  cidr_block        = "192.168.1.0/24"
  vpc_id            = aws_vpc.sample-vpc.id
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_subnet" "sample-subnet-b" {
  cidr_block        = "192.168.2.0/24"
  vpc_id            = aws_vpc.sample-vpc.id
  availability_zone = data.aws_availability_zones.available.names[1]
}

resource "aws_route_table" "sample-rt" {
  vpc_id = aws_vpc.sample-vpc.id

  tags = {
    Name = "sample-route-table"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.sample-rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.sample-igw.id
}

resource "aws_route_table_association" "sample-subnet-a" {
  subnet_id      = aws_subnet.sample-subnet-a.id
  route_table_id = aws_route_table.sample-rt.id
}

resource "aws_route_table_association" "sample-subnet-b" {
  subnet_id      = aws_subnet.sample-subnet-b.id
  route_table_id = aws_route_table.sample-rt.id
}