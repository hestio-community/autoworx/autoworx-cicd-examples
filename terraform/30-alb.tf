resource "aws_alb" "sample-alb" {
  name            = local.config.alb_name
  internal        = false
  security_groups = [aws_security_group.sample-alb-sg.id]
  subnets         = [aws_subnet.sample-subnet-a.id, aws_subnet.sample-subnet-b.id]
}

resource "aws_alb_target_group" "sample-alb-tg" {
  name        = local.config.alb_target_group_name
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = aws_vpc.sample-vpc.id
  target_type = "ip"

  health_check {
    enabled             = true
    interval            = 30
    path                = "/"
    timeout             = 5
    healthy_threshold   = 3
    unhealthy_threshold = 3
    protocol            = "HTTP"
    port                = "8080"
  }
}

resource "aws_alb_listener" "sample-alb-listener" {
  load_balancer_arn = aws_alb.sample-alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.sample-alb-tg.arn
  }
}