# Provider config

terraform {

  required_version = "~> 1.3"


  required_providers {
    aws = {
      version = "~> 4.0"
      source  = "hashicorp/aws"
    }

    null = {
      version = "~> 2.1"
      source  = "hashicorp/null"
    }

    template = {
      version = "~> 2.1"
      source  = "hashicorp/template"
    }

  }

}

