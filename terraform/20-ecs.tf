resource "aws_ecs_cluster" "ecs_cluster" {
  name = local.config.ecs_cluster_name
}

resource "aws_ecs_task_definition" "ecs_cluster" {
  family                   = local.config.app_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "512"

  container_definitions = jsonencode([
    {
      name        = local.config.app_name
      image       = local.config.container_image
      environment = local.config.container_envs
      portMappings = [
        {
          containerPort = 8080
          hostPort      = 8080
          protocol      = "tcp"
        }
      ]
    }
  ])
}

resource "aws_ecs_service" "sample-ecs-service" {
  name            = local.config.ecs_service_name
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.ecs_cluster.arn
  desired_count   = 1

  launch_type      = "FARGATE"
  platform_version = "LATEST"
  deployment_controller {
    type = "ECS"
  }

  network_configuration {
    subnets          = [aws_subnet.sample-subnet-a.id, aws_subnet.sample-subnet-b.id]
    security_groups  = [aws_security_group.sample-app-sg.id]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.sample-alb-tg.arn
    container_name   = local.config.app_name
    container_port   = 8080
  }

  depends_on = [aws_alb_listener.sample-alb-listener]
}
